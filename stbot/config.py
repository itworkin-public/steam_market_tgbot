from pathlib import Path

from pydantic import BaseSettings


MarketItem = tuple[str, float]


MARKET_ITEMS = (
    ("Paris 2023 Mirage Souvenir Package"             , 0.00000000001),
    ("Sticker | FaZe Clan (Holo) | Paris 2023"        , 0.00000000001),
    ("Sticker | Team Liquid (Holo) | Paris 2023"      , 0.00000000001),
    ("Sticker | MOUZ (Holo) | Paris 2023"             , 0.00000000001),
    ("Sticker | Complexity Gaming (Holo) | Paris 2023", 0.00000000001),
    ("Sticker | G2 Esports (Holo) | Paris 2023"       , 0.00000000001),
    ("Sticker | forZe eSports (Holo) | Paris 2023"    , 0.00000000001),
    ("Sticker | paiN Gaming (Holo) | Paris 2023"      , 0.00000000001),
    ("Sticker | Apeks (Holo) | Paris 2023"            , 0.00000000001),
    ("Sticker | Fnatic (Holo) | Paris 2023"           , 0.00000000001),
    ("Sticker | Natus Vincere (Holo) | Paris 2023"    , 0.00000000001),
    ("Sticker | FURIA (Holo) | Paris 2023"            , 0.00000000001),
    ("Sticker | Vitality (Holo) | Paris 2023"         , 0.00000000001),
    ("Sticker | Heroic (Holo) | Paris 2023"           , 0.00000000001),
    ("Sticker | 9INE (Holo) | Paris 2023"             , 0.00000000001),
)


class BotConfig(BaseSettings):
    TOKEN: str
    USERS_FILE: Path = Path(__file__).parents[1].absolute() / "users.txt"

    class Config:
        env_file = Path(__file__).parents[1].absolute() / ".env"
        print(env_file)


BOT_CONFIG = BotConfig() # type:ignore
