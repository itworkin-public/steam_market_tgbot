from aiogram.bot import Bot
from aiogram.dispatcher import Dispatcher
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.utils.executor import Executor
from aiogram.types import Message

from emoji import emojize

from ..config import BOT_CONFIG


class States(StatesGroup):
    waiting_state = State()


bot = Bot(BOT_CONFIG.TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
exc = Executor(dp)


@dp.message_handler(commands=["start"])
async def start_bot(message: Message):
    with open(BOT_CONFIG.USERS_FILE, "a") as f:
        f.write(str(message.from_user.id)+"\n")
    await message.reply(emojize(":brain:")+"Бот включен! При поступлении информации о снижении цены вам придет сообщение.")
