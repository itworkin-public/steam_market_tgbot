import emoji

from .bot import bot
from ..config import BOT_CONFIG


async def send_price_allert(
    item_name: str,
    lowest_price: float,
    median_price: float,
    percent: float,
):
    message = _make_info_message(item_name, lowest_price, median_price, percent)
    with open(BOT_CONFIG.USERS_FILE) as f:
        ids = tuple(map(int, f.readlines()))
        for id in ids:
            try:
                await bot.send_message(id, text=message)
            except:
                pass


def _make_info_message(
    item_name: str,
    lowest_price: float,
    median_price: float,
    percent: float,
):
    return (
        emoji.emojize(":brain:")
        +f"ВНИМАНИЕ"
        +emoji.emojize(":bangbang:")
        +f"\nИнформация по предмету:"
        +f"\n{item_name}"
        +f"\n    "
        +emoji.emojize(":white_check_mark:")
        +f"Самая низкая цена: {lowest_price}"
        +f"\n    "
        +emoji.emojize(":scales:")
        +f"Средняя цена: {median_price}"
        +f"\n    "
        +emoji.emojize(":mag_right:")
        +f"Указанный вами процент: {percent}"
    )
