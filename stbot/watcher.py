from typing import Iterable

from .config import MarketItem
from .steam import get_item_info
from .bot.utils import send_price_allert


async def watch_prices(market_items: Iterable[MarketItem], timeout: int = 60) -> None:
    from time import sleep

    print("start watching...")
    while True:
        for mitem, mpercent in market_items:
            mitem_info = get_item_info(mitem)
            if mitem_info is None:
                continue
            print(">  ", mitem_info)
            if not _is_valid_json(mitem_info):
                print("invalid data:", mitem_info)
                continue

            lowest = _str_price_to_float(mitem_info["lowest_price"])
            median = _str_price_to_float(mitem_info["median_price"])
            
            lowest_median_percent = 100*(median-lowest)/median
            print("-->", lowest_median_percent)
            if lowest_median_percent >= mpercent:
                await send_price_allert(mitem, lowest, median, mpercent)

        sleep(timeout)


def _is_valid_json(data: dict):
    return (
        data.get("lowest_price") is not None 
        and data.get("median_price") is not None
    )


def _str_price_to_float(price: str):
    price = (
        ""
        .join(char for char in price if char in "1234567890,")
        .replace(",", ".")
    )
    return float(price)

