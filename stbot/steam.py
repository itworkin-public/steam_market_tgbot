import requests as r

from .currencies import RUB

def get_item_info(item_name: str, currency_num: int = RUB, appid: int = 730) -> dict:
    try:
        url = (
            "https://steamcommunity.com/market/priceoverview/"
            f"?appid={appid}"
            f"&currency={currency_num}"
            f"&market_hash_name={item_name}"
        )
        response = r.get(url)
        return response.json()
    except Exception as err:
        print(f"Unable to fetch data about item:{item_name}"
              f"  [ERROR: {err}]")
        return {}
