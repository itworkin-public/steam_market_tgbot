import asyncio

from stbot.watcher import watch_prices
from stbot.config import MARKET_ITEMS



def run():
    asyncio.run(watch_prices(MARKET_ITEMS))


if __name__ == "__main__":
    run()
