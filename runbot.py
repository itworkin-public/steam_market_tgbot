from stbot.bot.bot import dp, exc


def run():
    exc.start_polling(dp)


if __name__ == "__main__":
    run()
